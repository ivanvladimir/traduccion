# traducción

Repositorio principal para ambiente de experimentación en traducción automática de Lenguas Originarias de Mexico

# Agradecimientos

Agradecemos el apoyo por parte del proyecto “Traducción automática para lenguas ind´ıgenas de Mexico” PAPIIT-IA104420, UNAM.

Agradecemos a CONACYT por los recursos proporcionados a través de la Plataforma de Aprendizaje Profundo del Laboratorio de Supercomputación del INAOE para
Tecnologías del Lenguaje.
